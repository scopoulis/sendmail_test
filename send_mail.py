#!/usr/bin/env python3

"""
Author:  YanniS
Purpose: To complete assignment 4.1
"""

from os import path
import smtplib
from email.message import EmailMessage
import random
import time


"""
Read the exam.csv file
Create a dictionary with students' records
Each record has the student's e-mail as key
Actually we create a dictionary of dictionaries for clarity
"""

def get_roster(fname):

    if not path.exists(fname):
        print(f"file {fname} does not exist")
        return {}

    file = open(fname, 'r')
    roster ={}

    try:
        for line in file:
            columns = line.split(';')
            email   = columns[0]
            student = { 
                "first_name": columns[1],
                "last_name" : columns[2],
                "score_1"   : columns[3],
                "comment_1" : columns[4],
                "score_2"   : columns[5],
                "comment_2" : columns[6],
                "score_3"   : columns[7],
                "comment_3" : columns[8]
            }
            roster[email] = student

    except Exception as exc:
        file.close()
        roster = {}
        print(f"{fname}: malformed content")

    return roster


""" 
Send mail to every student
Using ethereal smtp server for demo purposes (checked with gmail too) 
"""

def send_mail_to_students(roster):

    if len(roster) == 0:
        return
    
    lucky = random.choice(list(roster.keys()))

    for address in roster.keys():

        server = smtplib.SMTP('smtp.ethereal.email', 587)
        
        msg = EmailMessage()
        msg['Subject'] ='Assignment_4.1 grades'
        msg['From'] = 'grader@digigov.grnet.gr'
        msg['To'] = address

        msg_content  = f"Dear {roster[address]['first_name']}\n\n"
        msg_content += f"Your score for the book assignment is broken down below by question number:\n\n"
        msg_content += f"1. {roster[address]['score_1']}% {roster[address]['comment_1']}\n"
        msg_content += f"2. {roster[address]['score_2']}% {roster[address]['comment_2']}\n"
        msg_content += f"3. {roster[address]['score_3']}% {roster[address]['comment_3']}\n"
        
        if address == lucky :
            msg_content += "\nYou’ve been randomly chosen to present a summary of the book in the next class."
            msg_content += "\nLooking forward to it!\n"
        
        msg.set_content(msg_content)
        
        try:
            server.starttls()
            server.login("odell46@ethereal.email", "rbpBF7mBJpNktjhZ3Y") #in plain view because is just ... a test account
            server.send_message(msg)
            time.sleep(0.2)
            server.quit()
        except Exception:
            print("mail to {address} failed!")


"""
Main program
"""

if __name__ == '__main__':

    roster = get_roster('exam.csv')
    send_mail_to_students(roster) 


        




