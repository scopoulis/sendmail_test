# sendmail_with_python

Solution to assignment 4.1 from Manning live-project "How to think about automating email"

- Reads the student-details and their scores with teacher's comments from a csv file 
- One of the students (randomly chosen) is assigned an extra duty 
- Sends e-mail to every student innforming them about their scores and extra duties (if applicable)
